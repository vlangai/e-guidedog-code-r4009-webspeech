#!/usr/bin/perl

use strict;
use warnings;
use utf8;
use URI::Escape;
use threads;
use Time::HiRes qw(gettimeofday);
my $filebase = '/tmp/gtts' . int(gettimeofday * 1000);

my ($filename, $text, $speed, $volume) = @ARGV;

my $tempo = 1.2;
$tempo = $tempo * (1 + $speed / 100) if ($speed);
if ($volume) {
  $volume = 1 + $volume / 100;
} else {
  $volume = 1;
}

my $debug = 0;
$text = uri_unescape($text);
utf8::decode($text);

# split string
my @spliters = ('。', '；', '，', '（', '）');
my @clauses = split(/,|;|\.|\n|\(|\)/, $text);
my @clauses2;
foreach my $clause (@clauses) {
  while (length($clause) > 48) {
    my $pos = -1;
    foreach my $spliter (@spliters) {
      utf8::decode($spliter);
      $pos = index($clause, $spliter);
      last if ($pos > 0 && $pos < 48);
    }

    $pos = 47 if ($pos < 0 || $pos >= 48);
    push @clauses2, substr($clause, 0, $pos + 1);
    $clause = substr($clause, $pos + 1);
  }

  push @clauses2, $clause
}

# fork threads
my $i = 0;
my @threads;
foreach my $clause (@clauses2) {
  $i++;
  print "getting $clause\n" if $debug;
  my $escaped_clause = $clause; #uri_escape_utf8($clause);
  my $thread = threads->create(sub {
    `wget --tries=1 --dns-timeout=3 --read-timeout=10 --connect-timeout=5 -q -U '' -O $filebase$i.mp3 'http://translate.google.cn/translate_tts?ie=UTF-8&tl=zh-CN&client=t&q=$escaped_clause'`;
  });
  push @threads, $thread;
  if ($#threads > 5) {
    pop(@threads)->join();
  }
}

# join threads
foreach my $thread (@threads) {
  print "joining $thread\n" if $debug;
  $thread->join();
}

# join mp3
my $files = '';
my $has_error = 0;
foreach my $j (1 .. $i) {
  if (!-z "$filebase$j.mp3") {
    $files .= $filebase . $j . '.mp3 ';
  } else {
    $has_error = 1;
  }
}
if ($files) {
  `sox -v $volume $files $filename tempo $tempo`;
} else {
  $has_error = 1;
}

# delete temporary files
`rm $filebase*`;

if ($has_error) {
  print -1;
} else {
  print 0;
}
