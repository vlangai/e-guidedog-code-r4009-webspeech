#!/usr/bin/perl

use strict;
use warnings;
use utf8;
use URI::Escape;
use threads;
use Time::HiRes qw(gettimeofday);
use POSIX;

# API document: http://ai.baidu.com/docs#/ASR-API/top

my ($filename, $lang, $cuid) = @ARGV;

my $dev_pid = 1536;
if ($lang && $lang eq 'Cantonese') {
	$dev_pid = 1637;
}

if (!$cuid) {
	$cuid = 0;
}

# get access_token
if ((! -s '/tmp/baidu_tts_access_token') ||
	(time() - (stat('/tmp/baidu_tts_access_token'))[9] > 2592000)) {
  `wget --quiet -O /tmp/baidu_tts_access_token 'https://openapi.baidu.com/oauth/2.0/token?grant_type=client_credentials&client_id=qpAVK08PQj46E2Dmd8xZGkSM&client_secret=6729b9e576d68b1245ee49460f2d5da3'`;
  # example output: {"access_token":"24.d28bc1aca1b4842ce42c1faca75f518c.2592000.1461826385.282335-7915833","session_key":"9mzdX+gKAvTnBCP8+vAaLm+R3X2Utb\/TdnAiepxYXh6djtXrUUvOiUiehB92N\/RiRUEdPJczdkBz4PriRRoz0FhSrlxO","scope":"public audio_tts_post wise_adapt lebo_resource_base lightservice_public hetu_basic lightcms_map_poi kaidian_kaidian","refresh_token":"25.cd0c621e1133ae4949226aaca798c38f.315360000.1774594385.282335-7915833","session_secret":"437717f67457bc290a802820df23ed3a","expires_in":2592000}
}

my $access_token = `cat /tmp/baidu_tts_access_token`;
if ($access_token =~ /access_token":"([^"]+)"/) {
  $access_token = $1;
} else {
  print 'fail to get access token';
  exit;
}

if ((-s $filename) > 0) {
	my $cmd = 'curl -s -X POST -H "Content-Type: audio/pcm;rate=16000" "http://vop.baidu.com/server_api?dev_pid=' . $dev_pid .
		'&cuid=' . $cuid . '&token=' . $access_token . '" --data-binary "@' .
		$filename . '"';
	#print "$cmd\n";
	my $ret = `$cmd`;
	print "$ret";
	# sample output: {"corpus_no":"6719306727464059395","err_msg":"success.","err_no":0,"result":["咋试下，"],"sn":"398718046861564460510"}
} else {
	print "$filename not found";
}