#!/usr/bin/perl

use strict;
use warnings;
use utf8;
use Digest::MD5;
use CGI qw(:standard);
use File::Path;
use Encode;
use File::Basename;
use URI::Escape;
use MIME::Base64;
use File::Slurp;

our $ekho_dir = '/var/www/ekho-7.0';
our $version = 'ekho-7.0';
our $port = 20473;
our $voice_id = 'm3';
#our $voice_id = 'g1';

# Base directory of this script on this server.
our $base_dir = '/var/cache/ekho';
our $whitelist = '/var/www/ekho.whitelist';

# Available servers that are capable of running TTS.
our @servers = ('localhost');
our $file_prefix = '';
our $lame = '/usr/bin/lame';

# By default, use both caching and multiple TTS servers when available.
our $cache = 1;
our $mtts = 1;
our $speed = 0;
our $pitch = 0;
our $volume = 0;
our $cmd = '';
our $callback = '';
our $referer = '';

# Sound returned when an error is retrieved.
our $error_string = "We're sorry, the system has experienced an error.";

# Various rewrites to make strings sound better when voiced by the Festival TTS.
our %letters = ('a' => 'Aiy', 'b' => 'Bee', 'd' => 'Deee', ' ' => 'space', 'r' => 'are', 'e' => 'Eee', '!' => 'bang', '#' => 'pound', '?' => 'question mark', ';' => 'semicolon', ':' => 'colon', '[' => 'left bracket', '\\' => 'back slash', ']' => 'right bracket', '^' => 'carat', '_' => 'underscore', '`' => 'reverse apostrophe', '|' => 'pipe', '~'=>'tilde', '"' => 'quote', '$' => 'dollar', '%' => 'percent', '&' => 'ampersand', '\'' => 'apostrophe', '(' => 'open paren', ')' => 'close paren', '*' => 'asterisk', '+' => 'plus', ',' => 'comma', '-' => 'dash', '.' => 'dot', '/' => 'slash', '{' => 'open curly bracket', '}' => 'close curly bracket', '"' => 'quote', 'bigham' => 'biggum', 'cse' => 'C S E', 'url' => 'U R L'
);

our $final_filename;
our $final_info_filename;

our $text = "";
if(!param()) {
  $text = "There was an error with the request to the speech server.";
} else {
  $cache = param('cache') if (defined param('cache'));
  $mtts = param('mmts') if (defined param('mmts'));
  if (defined param('speedDelta') &&
      param('speedDelta') >= -50 && param('speedDelta') <= 100) {
    $speed = param('speedDelta');
  }
  if (defined param('pitchDelta') &&
      param('pitchDelta') >= -100 && param('pitchDelta') <= 100) {
    $pitch = param('pitchDelta');
  }
  if (defined param('volumeDelta') &&
      param('volumeDelta') >= -100 && param('volumeDelta') <= 100) {
    $volume = param('volumeDelta');
  }
  $cmd = param('cmd') if (defined param('cmd'));
  $callback = param('callback') if (defined param('callback'));
  $text = param('text');

  if (defined param('voice')) {
    my $voice = param('voice');
    if ($voice eq 'GoogleMandarin') {
      $port = 80;
      $voice_id = 'g1';
    } elsif ($voice eq 'EkhoMandarin' || $voice eq 'Mandarin') {
    } elsif ($voice eq 'EkhoCantonese' || $voice eq 'Cantonese') {
      $port = 20463;
      $voice_id = 'c3';
    } elsif ($voice eq 'BaiduMandarinMale') {
      $port = 80;
      $voice_id = 'b1';
    } elsif ($voice eq 'BaiduMandarinFemale') {
      $voice_id = 'b2';
    } elsif ($voice eq 'iflytek') {
      $port = 80;
      $voice_id = 'f1';
    } elsif ($voice eq 'iflytekXiaomei') {
      $port = 80;
      $voice_id = 'f2';
    }
  }

  if (defined param('referer')) {
    $referer = param('referer');
  }
}

$file_prefix .= 's' . $speed if ($speed);
$file_prefix .= 'p' . $pitch if ($pitch);
$file_prefix .= 'v' . $volume if ($volume);
#$cache = 0; # remove this when stable

# decode MS %u infamous non-standard encoding in URL
while ($text =~ /([^%]*)%u(....)(.*)/) {
  $text = $1 . chr(hex("0x$2")) . $3;
}
$text =~ s/\"//g;

logRequest($text, $port, $voice_id);
sendTTSToClient($text, $port, $voice_id, $cache, $mtts);

sub inWhiteList {
  my ($url) = @_;

  # blacklist
  if ($url =~ /jycatering.com/ ||
    $url =~ /ihanzipal.com/ ||
    $url =~ /100jed.com/ ||
    $url =~ /www.fjsmzt.gov.cn/ ||
    $url =~ /www.sslj1.0.com/ ||
    index($url, '120.40.102.240') >= 0 ||
    index($url, '61.129.251.21') >= 0 ||
    $url =~ /www.sm.gov.cn/) {
    return 0;
  }
  
  return 1; # disable white list 

  open(WHITELIST, '<', $whitelist) || return 0;
  while (my $line = <WHITELIST>) {
    chomp($line);
    if ($line && $url =~ /$line/) {
      return 1;
    }
  }
  close(WHITELIST);
  return 0;
}

sub logRequest {
  my ($text, $port, $voice_id) = @_;
  my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
  $year += 1900;
  ++$mon;
  my $log = "/var/log/ekho/web_request.$voice_id";
  if (-e $log && -s $log > 1000000) {
    for (my $i = 4; $i > 0; --$i) {
      if (-e "$log.$i") {
        rename("$log.$i", "$log." . ($i + 1));
      }
    }
    rename($log, "$log.1");
  }
  my $from = $ENV{'HTTP_REFERER'};
  $from = 'unknown' if (not $from);

  if ((!inWhiteList($from)) || ($from eq 'unknown' && (
      index($text, Encode::encode('utf-8', '中断')) >= 0 ||
      index($text, Encode::encode('utf-8', '故障')) >= 0))) {
    error('You have reached limit of free service. Please contact http://www.eguidedog.net/WebSpeech.php for commercial service.');
    #print header();
    #header("Location: $from");
  }

  if ($from eq 'unknown' && $referer) {
    $from = $referer;
  }

  open(ACCESS_LOG, '>>', $log) || return;
  printf ACCESS_LOG "[%4d%02d%02d-%02d:%02d:%02d] [%s] [%s] %s\n",
       $year, $mon, $mday, $hour, $min, $sec, $from, $port, $text;
  close(ACCESS_LOG);
}

############################################################################
# Writes the voiced MP3 of the passed string ($text) to the client.
############################################################################
sub sendTTSToClient {
  my ($text, $port, $voice_id, $cache, $mtts) = @_;

  $text =~ s/\s/ /g;
  $text =~ s/\"//g;  #"
  $text =~ s/\s&\s/ and /;
  if($letters{$text}) {
    $text = $letters{$text};
  }

  $text = trim($text);

  $text =~ s/\s&\s/ and /gi;
  $text =~ s/gmail/gee-mail/gi;
  $text =~ s/email/ee-mail/gi;
  $text =~ s/webanywhere/web anywhere/gi;
  $text =~ s/ctrl/control/gi;
  $text =~ s/eGuideDog/e-guidedog/gi;

  # Constructs a filename based on the MD5 of the text.
  my $md5 = Digest::MD5->new;
  $md5->add(encode_utf8($text));
  my $filename = $md5->b64digest;
  $filename =~ s/[\/+\s]/_/g;
  my $lc_filename = lc($filename);

  my $type = 'mp3';
  $type = 'ogg' if ($cmd eq 'SAVEOGG');
  $type = 'sym' if ($cmd eq 'GETPHONSYMBOLS');

  my $first_dir = substr($lc_filename, 0, 1);
  my $second_dir = substr($lc_filename, 1, 1);
  my $third_dir = substr($lc_filename, 2, 1);

  my $final_dir = "$base_dir/$version-$voice_id/$first_dir/$second_dir/$third_dir/";
  $final_filename = $final_dir . $file_prefix . $filename . '.' . $type;
  $final_info_filename = $final_dir . $file_prefix . $filename . ".txt";

  # Ensure that the final directory actually exists.
  mkpath $final_dir if (!-e $final_dir);

  my $generated_new = 'false';

  if((!(-e $final_filename)) || $cache == 0) {
    # Choose a TTS server.
    my $tts_server = $servers[0];
    if($mtts == 1) {
        if ($#servers >= 0) {
            my $rand = int(rand($#servers + 1));
            $tts_server = splice(@servers, $rand, 1);
        }
    }

    my $syscmd = '';
    if ($cmd eq 'GETPHONSYMBOLS') {
        $syscmd = "cd $ekho_dir && ./ekho --port $port -l --request \"$text\" -o $final_filename";
	system($syscmd);
    } else {
      if ($voice_id eq 'g1') {
        my $esc_text = uri_escape($text);
        my $ret = `perl /var/www/htdocs/cgi-bin/googletts.pl $final_filename '$esc_text' $speed $volume`;
        $cache = 0 if ($ret =~ /^[-]1/);
      } elsif ($voice_id eq 'b1') {
        my $esc_text = uri_escape($text);
        my $ret = `perl /var/www/htdocs/cgi-bin/baidutts.pl $final_filename '$esc_text' $speed $volume 1`;
      } elsif ($voice_id eq 'b2') {
        my $esc_text = uri_escape($text);
        my $ret = `perl /var/www/htdocs/cgi-bin/baidutts.pl $final_filename '$esc_text' $speed $volume 0`;
      } elsif ($voice_id eq 'f1') {
        my $ret = `cd /var/www/htdocs/cgi-bin/iflytek/ && perl iflytek.pl $final_filename "$text" $speed $volume`;
      } elsif ($voice_id eq 'f2') {
        my $ret = `cd /var/www/htdocs/cgi-bin/iflytek/ && perl iflytek_cantonese.pl $final_filename "$text" $speed $volume`;
      } else {
        $syscmd = "cd $ekho_dir && ./ekho --port $port --speed $speed --pitch $pitch --volume $volume -t $type --request \"$text\" -o $final_filename";
        system($syscmd);
      }
    }

    if (! -e $final_filename || -z $final_filename) {
      $syscmd = "cd $ekho_dir && ./ekho --port $port --speed $speed --pitch $pitch --volume $volume -t $type --request \"$text\" -o $final_filename";
      system($syscmd);
    }
    if (! -e $final_filename) {
      logRequest('[Error]' . $syscmd, $port, $voice_id);
    }
  }

  # Write the file back to the user.
  writeFileToClient($final_filename);

  `rm $final_filename` if (!$cache);
}

#########################################################################################
# Called when an error occurs.
#########################################################################################
our $returned_error = 0;
sub returnErrorSound() {
  if($returned_error) {
    sendTTSToClient($error_string, 0, 1);
  } else {
#    error("Sending error failed.");
  }
}

##############################################################################################
# Writes out the passed in file with the provided length to the client.
##############################################################################################
sub writeFileToClient($) {
  my ($final_filename, $total_millis) = @_;

  if(-e $final_filename) {
    my $buff;
    if ($cmd eq 'SAVEMP3') {
      print "Content-Type: audio/mpeg\n";
      print "Content-Disposition: attachment; filename=output.mp3\n";
    } elsif ($cmd eq 'SAVEOGG') {
      print "Content-Type: application/ogg\n";
      print "Content-Disposition: attachment; filename=output.ogg\n";
    } elsif ($cmd eq 'GETPHONSYMBOLS') {
      print "Content-Type: text/plain\n";
    } elsif ($cmd eq 'BASE64') { # return mp3 encoded in base64
      print "Content-Type: text/javascript\n";
      print "Final-name: base64" . $voice_id . basename($final_filename) . "\n";
      my $content = read_file($final_filename);
      my $base64 = encode_base64($content, '');
      print "Content-Length: " . (length($base64) + length($callback) + 4) . "\n";
      print "Expires: Thu, 31 Dec 2037 04:00:25 GMT\n\n"; 
      print "$callback('";
      print $base64;
      print "')";
      return;
    } else {
      print "Content-Type: audio/mpeg\n";
      print "Final-name: " . $voice_id . basename($final_filename) . "\n";
    }


    if ($cmd eq 'GETPHONSYMBOLS') {
      print "Content-Length: " . ((-s $final_filename) + length($callback) + 4) . "\n";
    } else {
      print "Content-Length: " . (-s $final_filename) . "\n";
    }

#    print "Sound-length: " . $total_millis . "\n";
    print "Expires: Thu, 31 Dec 2037 04:00:25 GMT\n\n"; 

    if ($cmd eq 'GETPHONSYMBOLS') {
      print "$callback('";
    }

    open(FILE, $final_filename);
    while(my $re = read(FILE, $buff, 4096)) {
      my $total_re += $re;
      print $buff;
    }
    close(FILE);

    if ($cmd eq 'GETPHONSYMBOLS') {
      print "')";
    }
  } else {
    # TODO:  Handle case where the speech server goes down.
    # Should return a static file that say something like "Speech server is down."
  }
}

#############################################################################################
# Trims a string of whitespace at beginning/end.
#############################################################################################
sub trim($) {
  my $string = shift;
  $string =~ s/^\s+//;
  $string =~ s/\s+$//;
  return $string;
}

#######################################################################
# Reports the error passed to it.
#######################################################################
sub error() {
  my $error = shift;

  print "Content-type: text/html\n\n";
  print "ERROR: " . $error;

  exit(0);
}

0;
