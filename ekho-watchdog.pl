#!/usr/bin/perl

use strict;
use warnings;
use File::Path;

my @paths = ('/var/www/ekho-5.7', '/var/www/ekho-5.7');
my @ports = (20471, 20461);
my @commands = (
	'./ekho --server -v Mandarin --port 20471',
	'./ekho --server -v Cantonese --port 20461',
);

for my $i (0 .. $#ports) {
#    my $file = "/tmp/ekho-$me.$i.wav";
#    `rm $file`;
#    `cd $paths[$i] && ./ekho --port $ports[$i] -o $file --request 01`;
#    `sync`;
#    if ((! -e $file) || (-s $file) == 0) {
	my $memory = `ps aux|grep "$commands[$i]" | grep -v nohup | grep -v grep | head -n 1 | awk '{print \$5}'`;
	chomp($memory);
	if (!$memory || $memory > 300000) {
		`ps -ef | grep "ekho --server" | grep "port $ports[$i]" | grep -v "grep" | awk '{print \$2}' | xargs kill -9`;
		system("cd $paths[$i] && nohup $commands[$i] &");
	}
}


