#!/usr/bin/perl

use strict;
use warnings;
use utf8;
use URI::Escape;
use threads;
use Time::HiRes qw(gettimeofday);

# API document: http://bbs.xfyun.cn/forum.php?mod=viewthread&tid=10454&page=1&extra=#pid45792

my ($filename, $text, $voice, $speed, $volume) = @ARGV;
my $tmpfile = '/tmp/iflytek_' . $voice . int(gettimeofday * 1000);

my $tempo = 1;
$tempo = $tempo * (1 + $speed / 100) if ($speed);
if ($volume) {
  $volume = 1 + $volume / 100;
} else {
  $volume = 1;
}

my $debug = 0;
$text = uri_unescape($text);
utf8::decode($text);
$text =~ s/"/\\"/u;

my $cmd = "env LD_LIBRARY_PATH=. ./iflytek $tmpfile \"$text\" $voice";
`$cmd`;

`sox -v $volume $tmpfile $filename tempo $tempo`;

# delete temporary files
`rm $tmpfile`;

