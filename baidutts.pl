#!/usr/bin/perl

use strict;
use warnings;
use utf8;
use URI::Escape;
use threads;
use Time::HiRes qw(gettimeofday);
use POSIX;

# API document: http://yuyin.baidu.com/docs/tts/136

my ($filename, $text, $speed, $volume, $gender) = @ARGV;

$text = uri_unescape($text);
utf8::decode($text);

# get access_token
if ((! -s '/tmp/baidu_tts_access_token') ||
	(time() - (stat('/tmp/baidu_tts_access_token'))[9] > 2592000)) {
  `wget --quiet -O /tmp/baidu_tts_access_token 'https://openapi.baidu.com/oauth/2.0/token?grant_type=client_credentials&client_id=qpAVK08PQj46E2Dmd8xZGkSM&client_secret=6729b9e576d68b1245ee49460f2d5da3'`;
  # example output: {"access_token":"24.d28bc1aca1b4842ce42c1faca75f518c.2592000.1461826385.282335-7915833","session_key":"9mzdX+gKAvTnBCP8+vAaLm+R3X2Utb\/TdnAiepxYXh6djtXrUUvOiUiehB92N\/RiRUEdPJczdkBz4PriRRoz0FhSrlxO","scope":"public audio_tts_post wise_adapt lebo_resource_base lightservice_public hetu_basic lightcms_map_poi kaidian_kaidian","refresh_token":"25.cd0c621e1133ae4949226aaca798c38f.315360000.1774594385.282335-7915833","session_secret":"437717f67457bc290a802820df23ed3a","expires_in":2592000}
}

my $access_token = `cat /tmp/baidu_tts_access_token`;
if ($access_token =~ /access_token":"([^"]+)"/) {
  $access_token = $1;
} else {
  print 'fail to get access token';
  exit;
}

# convert speed(-100, 100) to 1-9
$speed = floor($speed / 25 + 5);

# convert volume(-100, 100) to 0-9
$volume = floor($speed / 20 + 5);
$volume = 9 if ($volume > 9);

`wget --quiet -O $filename "http://tsn.baidu.com/text2audio?tex=$text&lan=zh&cuid=00:16:3e:00:52:64&ctp=1&tok=$access_token&spd=$speed&vol=$volume&per=$gender"`;
if (-s $filename < 150) {
  # probably access token expired
  `wget --quiet -O /tmp/baidu_tts_access_token 'https://openapi.baidu.com/oauth/2.0/token?grant_type=client_credentials&client_id=qpAVK08PQj46E2Dmd8xZGkSM&client_secret=6729b9e576d68b1245ee49460f2d5da3'`;
  `wget --quiet -O $filename "http://tsn.baidu.com/text2audio?tex=$text&lan=zh&cuid=00:16:3e:00:52:64&ctp=1&tok=$access_token&spd=$speed&vol=$volume&per=$gender"`;
}
